# GeometryDashClone
The main idea of the project is a ball that moves around the level on platforms.<br/>
Platforms have different lengths and distances between them.<br/>
By clicking on the screen or lmb, gravity for the ball changes direction<br/>

# Illustrations

![image](/uploads/bb2ef65a2a47180d59bf0926cd7c9602/image.png)

## The amount of time spent on development
* Approximately 1.5 - 2 hours were spent on the basic functionality
* Approximately 1 - 1.5 spent on user interface design and player effect
* Approximately 1 hour for UI programming
